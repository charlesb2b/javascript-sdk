import { Client } from '../client'

export interface CharlesBaseHandlerOptions {
  endpoint: string
  base: string
}

export class CharlesBaseHandler {
  private handlerOptions: CharlesBaseHandlerOptions
  private client: Client

  constructor(http: Client, handlerOptions: CharlesBaseHandlerOptions) {
    this.client = http
    this.handlerOptions = handlerOptions
  }
}
