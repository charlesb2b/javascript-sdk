import { BaseError } from './base-error'

export class UninstantiatedClient extends BaseError {
  public name = 'UninstantiatedClient'
  constructor(
    public message: string = 'Cannot instantiate API without instantiated HTTP client',
    properties?: any
  ) {
    super(message, properties)
  }
}
