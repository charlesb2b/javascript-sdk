import * as dotenv from 'dotenv'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import qs from 'qs'
dotenv.config()
import { v0 } from '../../src/charles-js'
import { initThInstance } from '../util'

const legacyId = '4564'

const mock = new MockAdapter(axios)
afterEach(() => {
  mock.reset()
})

const queryParams = {
  read: false,
  ignored: false,
  min_updated_at: new Date().toISOString()
}

describe('v0: Messages: can get all messages', () => {
  it("Charles's messages are instantiable", async () => {
    if (process.env.SYSTEM_TEST !== 'true') {
      mock.onPost('https://api.hello-charles.com/api/v0/users/login').reply(function (config) {
        return [
          200,
          {
            token: '',
            user: {
              id: '123',
              legacy_id: legacyId
            }
          }
        ]
      })

      mock.onGet(`https://api.hello-charles.com/api/v0/messages/${legacyId}`).reply(function (config) {
        return [
          200,
          {
            count: 1,
            results: [{}]
          }
        ]
      })
    }

    const th = await initThInstance()

    const Messages = th.messages()

    expect(Messages).toBeInstanceOf(v0.Messages)

    const { data } = await Messages.getAll()

    expect(Array.isArray(data)).toBe(true)
  })

  it('Can get all messges with filters', async () => {
    if (process.env.SYSTEM_TEST !== 'true') {
      mock.onPost('https://api.hello-charles.com/api/v0/users/login').reply(function (config) {
        return [
          200,
          {
            token: '',
            user: {
              id: '123',
              legacy_id: legacyId
            }
          }
        ]
      })

      mock
        .onGet(`https://api.hello-charles.com/api/v0/messages/${legacyId}?${qs.stringify(queryParams)}`)
        .reply(function (config) {
          return [
            200,
            {
              count: 1,
              results: [{}]
            }
          ]
        })
    }

    const th = await initThInstance()

    const Messages = th.messages()

    expect(Messages).toBeInstanceOf(v0.Messages)

    const { data } = await Messages.getAll(queryParams)

    expect(Array.isArray(data)).toBe(true)
  })

  it('rejects on status codes that are not 200', async () => {
    if (process.env.SYSTEM_TEST !== 'true') {
      mock.onPost('https://api.hello-charles.com/api/v0/users/login').reply(function (config) {
        return [
          200,
          {
            token: '',
            user: {
              id: '123',
              legacy_id: legacyId
            }
          }
        ]
      })

      mock.onGet(`https://api.hello-charles.com/api/v0/messages/${legacyId}`).reply(function (config) {
        return [205]
      })
    }

    try {
      const th = await initThInstance()
      await th.messages().getAll()
    } catch (err) {
      expect(err.name).toBe('MessagesFetchFailed')
    }
  })
})
